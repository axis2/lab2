#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>

#include "errhandle.h"


#define LINES_COUNT                 10
#define PARENT_PREFIX               "[parent]"
#define CHILD_PREFIX                "[child]"

void *printLines(void *v_args) {
    if (NULL == v_args) {
        errorfln("%s: unexpected argument value: null", __FUNCTION__);
        return NULL;
    }

    char *prefix = (char *) v_args;

    for (int i = 0; i < LINES_COUNT; i += 1) {
        printf("%s line #%d\n", prefix, i);
    }

    return NULL;
}

int main() {
    int errcode;

    pthread_t thread;
    errcode = pthread_create(&thread, NULL, printLines, CHILD_PREFIX);
    if (NO_ERROR != errcode) {
        errorfln("failed to spawn a thread: %s", strerror(errcode));
        return errcode;
    }

    errcode = pthread_join(thread, NULL);
    if (NO_ERROR != errcode) {
        errorfln("failed to join child thread: %s", strerror(errcode));
        return errcode;
    }

    printLines(PARENT_PREFIX);
    return EXIT_SUCCESS;
}
